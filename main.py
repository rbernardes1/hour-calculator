#Hours Calculator

from function_hour_calculator import hour_calculator

hours_1 = int(input('Enter hours 1:'))
min_1 = int(input('Enter minutes 1:'))
sec_1 = int(input('Enter seconds 1:'))

hours_2 = int(input('Enter hours 2:'))
min_2 = int(input('Enter minutes 2:'))
sec_2 = int(input('Enter seconds 2:'))

operation_type = input('Enter sum or subtract:')

total_hours, total_minutes, total_seconds = hour_calculator(hours_1, min_1, sec_1, hours_2, min_2, sec_2, operation_type)

print('{} hours, {} minutes e {} seconds = '.format(total_hours, total_minutes, total_seconds))


