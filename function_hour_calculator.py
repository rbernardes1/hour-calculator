def hour_calculator (hours_1, min_1, sec_1, hours_2, min_2, sec_2, operation_type):

    time1 = hours_1*60*60 + min_1*60 + sec_1
    time2 = hours_2*60*60 + min_2*60 + sec_2 

    if operation_type == 'sum':
        total_time = time1 + time2
    elif operation_type == 'subtract':
        total_time = time1 - time2
    else:
        print('OPERACAO INVALIDA')

    total_hours = total_time//(60*60)

    remaining_time = total_time - total_hours*60*60

    total_minutes = remaining_time//60
    
    total_seconds = remaining_time - total_minutes*60

    return total_hours, total_minutes, total_seconds